/** @external process.env.PORT **/

var fs = require('fs');
var path = require('path');
var http = require('http');
var express = require('express');
var serveStatic = require('serve-static');

var app = express();

app.get('/', function (req, res, next) {
    return fs.readFile(path.join(__dirname, './index.html'), 'utf8', function (err, html) {
        if (err) return next(err);
        return res.type('html').send(html);
    });
});

app.use(serveStatic(path.join(__dirname, './')));

function start () {
    var port = process.env.PORT || 3000;
    app.set('port', port);
    var server = http.createServer(app);
    server.listen(port);
}

if (require.main === module) {
    start();
}

module.exports = app;
